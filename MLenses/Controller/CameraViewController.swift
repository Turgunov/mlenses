//
//  ViewController.swift
//  MLenses
//
//  Created by Olimjon Turgunov on 06.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit
import AVFoundation
import CoreML
import Vision

enum FlashState {
    case on
    case off
}

class CameraViewController: UIViewController {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var roundedLabelView: RoundedShadowView!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var identificationLabel: UILabel!
    @IBOutlet weak var flashButton: RoundedShadowButton!
    @IBOutlet weak var captureImageView: RoundedShadowImageView!
    
    private var captureSession: AVCaptureSession!
    private var cameraOutput: AVCapturePhotoOutput!
    private var previewLayer: AVCaptureVideoPreviewLayer!
    
    private var photoData: Data?
    private var flashControlState: FlashState = .off
    
    private var speechSynthesizer = AVSpeechSynthesizer()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapCameraView))
        tapGesture.numberOfTapsRequired = 1
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .hd1920x1080
        
        let backCamera = AVCaptureDevice.default(for: .video)
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera!)
            if captureSession.canAddInput(input) {
                captureSession.addInput(input)
            }
            
            cameraOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddOutput(cameraOutput) {
                captureSession.addOutput(cameraOutput)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer.videoGravity = .resizeAspect
                previewLayer.connection?.videoOrientation = .portrait
                
                cameraView.layer.addSublayer(previewLayer)
                cameraView.addGestureRecognizer(tapGesture)
                captureSession.startRunning()
            }
            
        }catch {
            debugPrint(error)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        previewLayer.frame = cameraView.bounds
        speechSynthesizer.delegate = self
    }
    
    @objc func didTapCameraView() {
        cameraView.isUserInteractionEnabled = false
        spinner.startAnimating()
        spinner.isHidden = false
        
        let settings = AVCapturePhotoSettings()
        settings.previewPhotoFormat = settings.embeddedThumbnailPhotoFormat
        
        if flashControlState == .off {
            settings.flashMode = .off
        }else {
            settings.flashMode = .on
        }
        
        cameraOutput.capturePhoto(with: settings, delegate: self)
    }
    
    private func resultsMethod(request: VNRequest, error: Error?) {
        guard let results = request.results as? [VNClassificationObservation] else { return }
        for classification in results {
            if classification.confidence <= 0.5 {
                let uknownObjectMessage = "I'm not sure what's this. Please try again..."
                identificationLabel.text = uknownObjectMessage
                synthesizeSpeech(from: uknownObjectMessage)
                confidenceLabel.text = ""
                break
            }else {
                let identification = classification.identifier
                let confidence = Int(classification.confidence * 100)
                identificationLabel.text = identification
                confidenceLabel.text = "CONFIDENCE: \(confidence)%"
                synthesizeSpeech(from: "This looks like a \(identification) and I'm \(confidence)% sure")
                break
            }
        }
    }
    
    @IBAction func flashButtonPressed(_ sender: RoundedShadowButton) {
        switch flashControlState {
        case .off:
            sender.setTitle("FLASH ON", for: .normal)
            flashControlState = .on
        case .on:
            sender.setTitle("FLASH OFF", for: .normal)
            flashControlState = .off
        }
    }
    
    private func synthesizeSpeech(from string: String) {
        let speechUtterane = AVSpeechUtterance(string: string)
        speechSynthesizer.speak(speechUtterane)
    }
    
}

extension CameraViewController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if error != nil {
            debugPrint(error!)
        }else {
            photoData = photo.fileDataRepresentation()
            let image = UIImage(data: photoData!)
            
            do {
                let model = try VNCoreMLModel(for: SqueezeNet().model)
                let request = VNCoreMLRequest(model: model, completionHandler: resultsMethod)
                let handler = VNImageRequestHandler(data: photoData!)
                try handler.perform([request])
            }catch {
                debugPrint(error)
            }
            
            captureImageView.image = image
        }
    }
}

extension CameraViewController: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        cameraView.isUserInteractionEnabled = true
        spinner.stopAnimating()
    }
}

