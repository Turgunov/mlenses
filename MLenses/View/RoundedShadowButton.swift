//
//  RoundedShadowButton.swift
//  MLenses
//
//  Created by Olimjon Turgunov on 06.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class RoundedShadowButton: UIButton {
    override func awakeFromNib() {
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowRadius = 15
        layer.shadowOpacity = 0.75
        layer.cornerRadius = frame.size.height / 2
    }
    
}
